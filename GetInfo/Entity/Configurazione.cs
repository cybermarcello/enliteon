﻿using System;
using GetInfo.Entity;

namespace GetInfo {
	internal static class Configurazione {

		internal static versione VersioneDaUtilizzare = versione.v1_0;
		internal static string UUID_Sonda = "5220a274863933674311569db40f3d6816be317267d8fce4c97a6ab70393a9f0";
		internal static string URL_Servizio = "api.enliteon.com";
		internal static string connectionString = "Server=(local);Initial Catalog=Enliteon;User id=sa;Password=testtest123";
		internal static long IdCamera = -1;

		internal static bool scaricaCounter = false;
		internal static bool scaricaPolygon = false;
		internal static bool scaricaTransitions = false;
		internal static bool scaricaTracks = false;

		internal static bool runningCounter = false;
		internal static bool runningPolygon = false;
		internal static bool runningTransitions = false;
		internal static bool runningTracks = false;

	}
}
