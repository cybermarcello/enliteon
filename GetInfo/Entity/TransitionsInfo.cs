﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GetInfo.Entity {
	[DataContract]
	internal class TransitionsInfo {
		[DataMember(Name = "trackId")]
		internal string TrackId;

		[DataMember(Name = "in")]
		internal string ValIn;

		[DataMember(Name = "out")]
		internal string ValOut;

		[DataMember(Name = "timestamp")]
		internal string timeStamp;
	}
}
