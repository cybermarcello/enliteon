﻿using System;
using System.Runtime.Serialization;

namespace GetInfo.Entity {
	[DataContract]
	internal class CounterInfo {
		[DataMember(Name = "in")]
		internal string inCounting;

		[DataMember(Name = "out")]
		internal string outCounting;

		[DataMember(Name = "timestamp")]
		internal string timeStamp;
	}
}
