﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GetInfo.Entity {
	[DataContract]
	internal class TracksInfo {
		[DataMember(Name = "trackId")]
		internal string TrackId;

		[DataMember(Name = "x")]
		internal string PosX;

		[DataMember(Name = "y")]
		internal string PosY;

		[DataMember(Name = "height")]
		internal string PeopleHeight;

		[DataMember(Name = "timestamp")]
		internal string timeStamp;
	}
}
