﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace GetInfo.Entity {
	[DataContract]
	internal class PolygonInfo {
		[DataMember(Name = "count")]
		internal string Count;

		[DataMember(Name = "timestamp")]
		internal string timeStamp;

		[DataMember(Name = "trackIds")]
		internal List<string> TrackIds;
	}
}
