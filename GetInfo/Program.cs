﻿using System;
using System.Configuration;

namespace GetInfo {
	class Program {

		public static void Main(string[] args) {
			leggiConfigurazione();

			#region Gestione dei parametri della chiamata
			for (int i = 0; i < args.Length; i++) {
				string[] kv = args[i].Split('=');
				switch (kv[0].ToLower()) {
					case "/?":
						Console.WriteLine("I possibili parametri sono:");
						Console.WriteLine("");
						Console.WriteLine("/cn     Nome della stringa di connessione da utilizzare.");
						Console.WriteLine("/url    Indirizzo da chiamare per leggere i dati (es. api.enliteon.com).");
						Console.WriteLine("/uuid   Identificativo della sonda.");
						Console.WriteLine("/dati   Indica quali dati sono da scaricare. Valori ammessi");
						Console.WriteLine("        Counter     per scaricare i 'counter'");
						Console.WriteLine("        Polygon     per scaricare i 'polygon'");
						Console.WriteLine("        Tracks      per scaricare i 'tracks'");
						Console.WriteLine("        Transitions per scaricare i 'transitions'");
						Console.WriteLine("        *           per scaricare tutto");
						Console.WriteLine("        Si possono anche inserire più valori divisi da virgola");
						return;
					case "/cn":
						Configurazione.connectionString = ConfigurationManager.ConnectionStrings[kv[1]].ConnectionString;
						break;
					case "/url":
						Configurazione.URL_Servizio = kv[1];
						break;
					case "/uuid":
						Configurazione.URL_Servizio = kv[1];
						break;
					case "/dati":
						foreach (string s in kv[1].Split(',')) {
							switch (s.ToLower()) {
								case "*":
									Configurazione.scaricaCounter = true;
									Configurazione.scaricaPolygon = true;
									Configurazione.scaricaTracks = true;
									Configurazione.scaricaTransitions = true;
									break;
								case "counter":
									Configurazione.scaricaCounter = true;
									break;
								case "polygon":
									Configurazione.scaricaPolygon = true;
									break;
								case "tracks":
									Configurazione.scaricaTracks = true;
									break;
								case "transitions":
									Configurazione.scaricaTransitions = true;
									break;
							}
						}
						break;
				}
			}
			#endregion

			if (Configurazione.scaricaCounter) {
				Enliteon_V01.LeggiCounter();
			}
			if (Configurazione.scaricaPolygon) {
				Enliteon_V01.LeggiPolygon();
			}
			if (Configurazione.scaricaTracks) {
				Enliteon_V01.LeggiTracks();
			}
			if (Configurazione.scaricaTransitions) {
				Enliteon_V01.LeggiTransitions();
			}

			DateTime dtCheck = DateTime.MinValue;
			do {
				System.Threading.Thread.Sleep(60000);
				if (dtCheck.ToString("yyyyMMdd") != DateTime.Now.ToString("yyyyMMdd")) {
					dtCheck = DateTime.Now;
					Enliteon_V01.PuliziaDB();
				}
			} while (Configurazione.runningCounter || Configurazione.runningPolygon || Configurazione.runningTransitions || Configurazione.runningTracks);
		}

		#region leggiConfigurazione
		private static void leggiConfigurazione() {
			Configurazione.connectionString = ConfigurationManager.ConnectionStrings["SQLConnection"].ConnectionString;
			Configurazione.URL_Servizio = ConfigurationManager.AppSettings["ServizioUri"];
			Configurazione.UUID_Sonda = ConfigurationManager.AppSettings["UUID"];
			string DatiDaScaricare = ConfigurationManager.AppSettings["DatiDaScaricare"];
			if (DatiDaScaricare.Trim() == "*") {
				Configurazione.scaricaCounter = true;
				Configurazione.scaricaPolygon = true;
				Configurazione.scaricaTracks = true;
				Configurazione.scaricaTransitions = true;
			} else {
				foreach (string s in DatiDaScaricare.Split(',')) {
					switch (s.ToLower()) {
						case "*":
							Configurazione.scaricaCounter = true;
							Configurazione.scaricaPolygon = true;
							Configurazione.scaricaTracks = true;
							Configurazione.scaricaTransitions = true;
							break;
						case "counter":
							Configurazione.scaricaCounter = true;
							break;
						case "polygon":
							Configurazione.scaricaPolygon = true;
							break;
						case "tracks":
							Configurazione.scaricaTracks = true;
							break;
						case "transitions":
							Configurazione.scaricaTransitions = true;
							break;
					}
				}
			}
		}
		#endregion
	}
}
