﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using System.Runtime.Serialization;
using System.IO;
using System.Runtime.Serialization.Json;
using EnliteonLibraries.Common;
using GetInfo.Entity;

namespace GetInfo {
	internal static class Enliteon_V01 {
		private static Thread ThreadCounter = null;
		private static Thread ThreadPolygon = null;
		private static Thread ThreadTransitions = null;
		private static Thread ThreadTracks = null;
		private static LogLocation location = LogLocation.Disk | LogLocation.Console;

		#region Gestione Counter
		#region LancioLetturaCounter
		public static void LancioLetturaCounter() {
			MqttClient clientCounter;

			try {
				IPAddress address;
				IPHostEntry AddressIPDns;
				if (IPAddress.TryParse(Configurazione.URL_Servizio, out address)) {
					clientCounter = new MqttClient(IPAddress.Parse(address.ToString()));
				} else {
					//Invalid IP
					AddressIPDns = Dns.GetHostEntry(Configurazione.URL_Servizio);
					clientCounter = new MqttClient(IPAddress.Parse(AddressIPDns.AddressList[0].ToString()));
				}

				clientCounter.MqttMsgPublishReceived += ClientCounter_MqttMsgPublishReceived; ;
				clientCounter.MqttMsgPublished += ClientCounter_MqttMsgPublished;
				clientCounter.MqttMsgUnsubscribed += ClientCounter_MqttMsgUnsubscribed;

				RecuperaIdCamera();

				try {
					string clientId = Guid.NewGuid().ToString();
					clientCounter.Connect(clientId);

					try {
						String topic = "edge/" + Configurazione.UUID_Sonda + "/counter";
						clientCounter.Subscribe(new string[] { topic }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
					} catch (Exception ex) {
						Logger.Write(String.Format("[MQTT Client Exception] {0}", ex.Message), location);
					}

				} catch (Exception ex) {
					Logger.Write(String.Format("[MQTT Client Exception] {0}", ex.Message), location);
				}

			} catch (Exception ex) {
				Logger.Write(String.Format("[MQTT Client Exception] {0}", ex.Message), location);
			}
		}
		#endregion
		#region ClientCounter_MqttMsgUnsubscribed
		private static void ClientCounter_MqttMsgUnsubscribed(object sender, MqttMsgUnsubscribedEventArgs e) {
			try {
				Logger.Write(String.Format("[MQTT] UnSubscribe to the Topic: OK - MessageId: {0}", e.MessageId), location);
			} catch (Exception ex) {
				Logger.Write(String.Format("[MQTT UnSubscribe Exception] {0}", ex.Message), location);
			}
		}
		#endregion
		#region ClientCounter_MqttMsgPublished
		private static void ClientCounter_MqttMsgPublished(object sender, MqttMsgPublishedEventArgs e) {
			Logger.Write(String.Format("[MQTT] Reset People Counter: OK - MessageId: {0}, IsPublished: {1}", e.MessageId, e.IsPublished), location);
		}
		#endregion
		#region ClientCounter_MqttMsgPublishReceived (Ricezione della coda)
		private static void ClientCounter_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e) {
			CounterInfo deserializedData = new CounterInfo();
			try {
				MemoryStream ms = new MemoryStream(e.Message);
				DataContractJsonSerializer ser = new DataContractJsonSerializer(deserializedData.GetType());
				deserializedData = ser.ReadObject(ms) as CounterInfo;
				ms.Close();
			} catch (Exception ex) {
				Logger.Write(String.Format("[Exception Seserializzatore json]: {0}", ex.Message), location);
			}

			DbUtils myDB = new DbUtils(Configurazione.connectionString);
			myDB.CounterInserisci(Configurazione.IdCamera, System.Convert.ToDateTime(deserializedData.timeStamp), System.Convert.ToInt64(deserializedData.inCounting), System.Convert.ToInt64(deserializedData.outCounting));
		}
		#endregion
		#region LeggiCounter
		public static bool LeggiCounter() {
			try {
				if (ThreadCounter != null) {
					return true;
				}

				Configurazione.runningCounter = true;

				ThreadCounter = new Thread(new ThreadStart(Enliteon_V01.LancioLetturaCounter));
				ThreadCounter.SetApartmentState(ApartmentState.MTA);
				ThreadCounter.Start();

				return true;
			} catch (Exception ex) {
				Logger.Write(String.Format("[Exception] {0}", ex.Message), location);

				if (ThreadCounter != null) {
					if (ThreadCounter.ThreadState != ThreadState.Stopped) {
						ThreadCounter.Abort();
					}
					ThreadCounter = null;
				}
				Configurazione.runningCounter = false;
			}
			return false;
		}
		#endregion
		#endregion
		#region Gestione Polygon
		#region LancioLetturaPolygon
		public static void LancioLetturaPolygon() {
			MqttClient clientPolygon;

			try {
				IPAddress address;
				IPHostEntry AddressIPDns;
				if (IPAddress.TryParse(Configurazione.URL_Servizio, out address)) {
					clientPolygon = new MqttClient(IPAddress.Parse(address.ToString()));
				} else {
					//Invalid IP
					AddressIPDns = Dns.GetHostEntry(Configurazione.URL_Servizio);
					clientPolygon = new MqttClient(IPAddress.Parse(AddressIPDns.AddressList[0].ToString()));
				}

				clientPolygon.MqttMsgPublishReceived += ClientPolygon_MqttMsgPublishReceived; ; ;
				clientPolygon.MqttMsgPublished += ClientPolygon_MqttMsgPublished; ;
				clientPolygon.MqttMsgUnsubscribed += ClientPolygon_MqttMsgUnsubscribed; ;

				RecuperaIdCamera();

				try {
					string clientId = Guid.NewGuid().ToString();
					clientPolygon.Connect(clientId);

					try {
						String topic = "edge/" + Configurazione.UUID_Sonda + "/polygon";
						clientPolygon.Subscribe(new string[] { topic }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
					} catch (Exception ex) {
						Logger.Write(String.Format("[MQTT Client Exception] {0}", ex.Message), location);
					}

				} catch (Exception ex) {
					Logger.Write(String.Format("[MQTT Client Exception] {0}", ex.Message), location);
				}

			} catch (Exception ex) {
				Logger.Write(String.Format("[MQTT Client Exception] {0}", ex.Message), location);
			}
		}
		#endregion
		#region ClientPolygon_MqttMsgUnsubscribed
		private static void ClientPolygon_MqttMsgUnsubscribed(object sender, MqttMsgUnsubscribedEventArgs e) {
			try {
				Logger.Write(String.Format("[MQTT] UnSubscribe to the Topic: OK - MessageId: {0}", e.MessageId), location);
			} catch (Exception ex) {
				Logger.Write(String.Format("[MQTT UnSubscribe Exception] {0}", ex.Message), location);
			}
		}
		#endregion
		#region ClientPolygon_MqttMsgPublished
		private static void ClientPolygon_MqttMsgPublished(object sender, MqttMsgPublishedEventArgs e) {
			Logger.Write(String.Format("[MQTT] Reset People Counter: OK - MessageId: {0}, IsPublished: {1}", e.MessageId, e.IsPublished), location);
		}
		#endregion
		#region ClientPolygon_MqttMsgPublishReceived (Ricezione della coda)
		private static void ClientPolygon_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e) {
			string result = System.Text.Encoding.UTF8.GetString(e.Message);
			PolygonInfo deserializedData = new PolygonInfo();
			try {
				MemoryStream ms = new MemoryStream(e.Message);
				DataContractJsonSerializer ser = new DataContractJsonSerializer(deserializedData.GetType());
				deserializedData = ser.ReadObject(ms) as PolygonInfo;
				ms.Close();
			} catch (Exception ex) {
				Logger.Write(String.Format("[Exception Seserializzatore json]: {0}", ex.Message), location);
			}

			DbUtils myDB = new DbUtils(Configurazione.connectionString);
			myDB.PolygonInserisci(Configurazione.IdCamera, System.Convert.ToDateTime(deserializedData.timeStamp), System.Convert.ToUInt32(deserializedData.Count), deserializedData.TrackIds);
		}
		#endregion
		#region LeggiPolygon
		public static bool LeggiPolygon() {
			try {
				if (ThreadPolygon != null) {
					return true;
				}

				Configurazione.runningPolygon = true;

				ThreadPolygon = new Thread(new ThreadStart(Enliteon_V01.LancioLetturaPolygon));
				ThreadPolygon.SetApartmentState(ApartmentState.MTA);
				ThreadPolygon.Start();

				return true;
			} catch (Exception ex) {
				Logger.Write(String.Format("[Exception] {0}", ex.Message), location);

				if (ThreadPolygon != null) {
					if (ThreadPolygon.ThreadState != ThreadState.Stopped) {
						ThreadPolygon.Abort();
					}
					ThreadPolygon = null;
				}
			}
			return false;
		}
		#endregion
		#endregion
		#region Gestione Transitions
		#region LancioLetturaTransitions
		public static void LancioLetturaTransitions() {
			MqttClient clientTransitions;

			try {
				IPAddress address;
				IPHostEntry AddressIPDns;
				if (IPAddress.TryParse(Configurazione.URL_Servizio, out address)) {
					clientTransitions = new MqttClient(IPAddress.Parse(address.ToString()));
				} else {
					//Invalid IP
					AddressIPDns = Dns.GetHostEntry(Configurazione.URL_Servizio);
					clientTransitions = new MqttClient(IPAddress.Parse(AddressIPDns.AddressList[0].ToString()));
				}

				clientTransitions.MqttMsgPublishReceived += ClientTransitions_MqttMsgPublishReceived;
				clientTransitions.MqttMsgPublished += ClientTransitions_MqttMsgPublished;
				clientTransitions.MqttMsgUnsubscribed += ClientTransitions_MqttMsgUnsubscribed;

				RecuperaIdCamera();

				try {
					string clientId = Guid.NewGuid().ToString();
					clientTransitions.Connect(clientId);

					try {
						String topic = "edge/" + Configurazione.UUID_Sonda + "/transactions";
						clientTransitions.Subscribe(new string[] { topic }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
					} catch (Exception ex) {
						Logger.Write(String.Format("[MQTT Client Exception] {0}", ex.Message), location);
					}

				} catch (Exception ex) {
					Logger.Write(String.Format("[MQTT Client Exception] {0}", ex.Message), location);
				}

			} catch (Exception ex) {
				Logger.Write(String.Format("[MQTT Client Exception] {0}", ex.Message), location);
			}
		}
		#endregion
		#region ClientTransitions_MqttMsgUnsubscribed
		private static void ClientTransitions_MqttMsgUnsubscribed(object sender, MqttMsgUnsubscribedEventArgs e) {
			try {
				Logger.Write(String.Format("[MQTT] UnSubscribe to the Topic: OK - MessageId: {0}", e.MessageId), location);
			} catch (Exception ex) {
				Logger.Write(String.Format("[MQTT UnSubscribe Exception] {0}", ex.Message), location);
			}
		}
		#endregion
		#region ClientTransitions_MqttMsgPublished
		private static void ClientTransitions_MqttMsgPublished(object sender, MqttMsgPublishedEventArgs e) {
			Logger.Write(String.Format("[MQTT] Reset People Counter: OK - MessageId: {0}, IsPublished: {1}", e.MessageId, e.IsPublished), location);
		}
		#endregion
		#region ClientTransitions_MqttMsgPublishReceived
		private static void ClientTransitions_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e) {
			string result = System.Text.Encoding.UTF8.GetString(e.Message);
			TransitionsInfo deserializedData = new TransitionsInfo();
			try {
				MemoryStream ms = new MemoryStream(e.Message);
				DataContractJsonSerializer ser = new DataContractJsonSerializer(deserializedData.GetType());
				deserializedData = ser.ReadObject(ms) as TransitionsInfo;
				ms.Close();
			} catch (Exception ex) {
				Logger.Write(String.Format("[Exception Seserializzatore json]: {0}", ex.Message), location);
			}

			DbUtils myDB = new DbUtils(Configurazione.connectionString);
			myDB.TransitionsInserisci(Configurazione.IdCamera, System.Convert.ToDateTime(deserializedData.timeStamp), System.Convert.ToUInt32(deserializedData.TrackId), System.Convert.ToUInt32(deserializedData.ValIn), System.Convert.ToUInt32(deserializedData.ValOut));
		}
		#endregion
		#region LeggiTransitions
		public static bool LeggiTransitions() {
			try {
				if (ThreadTransitions != null) {
					return true;
				}

				Configurazione.runningTransitions = true;

				ThreadTransitions = new Thread(new ThreadStart(Enliteon_V01.LancioLetturaTransitions));
				ThreadTransitions.SetApartmentState(ApartmentState.MTA);
				ThreadTransitions.Start();

				return true;
			} catch (Exception ex) {
				Logger.Write(String.Format("[Exception] {0}", ex.Message), location);

				if (ThreadTransitions != null) {
					if (ThreadTransitions.ThreadState != ThreadState.Stopped) {
						ThreadTransitions.Abort();
					}

					ThreadTransitions = null;
				}
			}
			return false;
		}
		#endregion
		#endregion
		#region Gestione Tracks
		#region LancioLetturaTracks
		public static void LancioLetturaTracks() {
			MqttClient clientTracks;

			try {
				IPAddress address;
				IPHostEntry AddressIPDns;
				if (IPAddress.TryParse(Configurazione.URL_Servizio, out address)) {
					clientTracks = new MqttClient(IPAddress.Parse(address.ToString()));
				} else {
					//Invalid IP
					AddressIPDns = Dns.GetHostEntry(Configurazione.URL_Servizio);
					clientTracks = new MqttClient(IPAddress.Parse(AddressIPDns.AddressList[0].ToString()));
				}

				clientTracks.MqttMsgPublishReceived += ClientTracks_MqttMsgPublishReceived; ; ; ;
				clientTracks.MqttMsgPublished += ClientTracks_MqttMsgPublished; ; ;
				clientTracks.MqttMsgUnsubscribed += ClientTracks_MqttMsgUnsubscribed; ; ;

				RecuperaIdCamera();

				try {
					string clientId = Guid.NewGuid().ToString();
					clientTracks.Connect(clientId);

					try {
						String topic = "edge/" + Configurazione.UUID_Sonda + "/tracks";
						clientTracks.Subscribe(new string[] { topic }, new byte[] { MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE });
					} catch (Exception ex) {
						Logger.Write(String.Format("[MQTT Client Exception] {0}", ex.Message), location);
					}

				} catch (Exception ex) {
					Logger.Write(String.Format("[MQTT Client Exception] {0}", ex.Message), location);
				}

			} catch (Exception ex) {
				Logger.Write(String.Format("[MQTT Client Exception] {0}", ex.Message), location);
			}
		}
		#endregion
		#region ClientTracks_MqttMsgUnsubscribed
		private static void ClientTracks_MqttMsgUnsubscribed(object sender, MqttMsgUnsubscribedEventArgs e) {
			try {
				Logger.Write(String.Format("[MQTT] UnSubscribe to the Topic: OK - MessageId: {0}", e.MessageId), location);
			} catch (Exception ex) {
				Logger.Write(String.Format("[MQTT UnSubscribe Exception] {0}", ex.Message), location);
			}
		}
		#endregion
		#region ClientTracks_MqttMsgPublished
		private static void ClientTracks_MqttMsgPublished(object sender, MqttMsgPublishedEventArgs e) {
			Logger.Write(String.Format("[MQTT] Reset People Counter: OK - MessageId: {0}, IsPublished: {1}", e.MessageId, e.IsPublished), location);
		}
		#endregion
		#region ClientTracks_MqttMsgPublishReceived (Ricezione della coda)
		private static void ClientTracks_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e) {
			string result = System.Text.Encoding.UTF8.GetString(e.Message);
			TracksInfo deserializedData = new TracksInfo();
			try {
				MemoryStream ms = new MemoryStream(e.Message);
				DataContractJsonSerializer ser = new DataContractJsonSerializer(deserializedData.GetType());
				deserializedData = ser.ReadObject(ms) as TracksInfo;
				ms.Close();
			} catch (Exception ex) {
				Logger.Write(String.Format("[Exception Seserializzatore json]: {0}", ex.Message), location);
			}

			DbUtils myDB = new DbUtils(Configurazione.connectionString);
			myDB.TracksInserisci(Configurazione.IdCamera, System.Convert.ToDateTime(deserializedData.timeStamp), System.Convert.ToUInt32(deserializedData.TrackId), System.Convert.ToUInt32(deserializedData.PosX), System.Convert.ToUInt32(deserializedData.PosY), System.Convert.ToUInt32(deserializedData.PeopleHeight));
		}
		#endregion
		#region LeggiTracks
		public static bool LeggiTracks() {
			try {
				if (ThreadTracks != null) {
					return true;
				}

				Configurazione.runningTracks = true;

				ThreadTracks = new Thread(new ThreadStart(Enliteon_V01.LancioLetturaTracks));
				ThreadTracks.SetApartmentState(ApartmentState.MTA);
				ThreadTracks.Start();

				return true;
			} catch (Exception ex) {
				Logger.Write(String.Format("[Exception] {0}", ex.Message), location);

				if (ThreadTracks != null) {
					if (ThreadTracks.ThreadState != ThreadState.Stopped) {
						ThreadTracks.Abort();
					}

					ThreadTracks = null;
				}
			}
			return false;
		}
		#endregion
		#endregion

		#region RecuperaIdCamera
		private static void RecuperaIdCamera() {
			DbUtils myDB = new DbUtils(Configurazione.connectionString);
			if (Configurazione.IdCamera == -1) {
				Configurazione.IdCamera = myDB.getCameraID(Configurazione.UUID_Sonda);
			}
		}
		#endregion

		#region PuliziaDB
		public static void PuliziaDB() {
			DbUtils myDB = new DbUtils(Configurazione.connectionString);
			myDB.PurgeDB(Configurazione.UUID_Sonda);
		}
		#endregion

	}
}
