﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enliteon_Entity {
	public partial class reportLine {
		public Nullable<int> T { get; set; }
		public Nullable<System.DateTime> Period { get; set; }
		public int StepID { get; set; }
	}
}
