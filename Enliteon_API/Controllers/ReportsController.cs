﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Enliteon_Entity;

namespace Enliteon_API.Controllers {
	public class ReportsController : ApiController {

		[HttpPost]
		public List<reportLine> PostStatCounter(requestCounter request)
		{
			List<reportLine> reportList = new List<reportLine>();

			reportList.Add(new reportLine() { Period = new DateTime(2018, 7, 1), StepID = 1, T = 5 });
			reportList.Add(new reportLine() { Period = new DateTime(2018, 7, 2), StepID = 1, T = 15 });
			reportList.Add(new reportLine() { Period = new DateTime(2018, 7, 3), StepID = 1, T = 10 });
			reportList.Add(new reportLine() { Period = new DateTime(2018, 7, 4), StepID = 1, T = 52 });
			reportList.Add(new reportLine() { Period = new DateTime(2018, 7, 5), StepID = 1, T = 25 });

			reportList.Add(new reportLine() { Period = new DateTime(2018, 7, 1), StepID = 2, T = 3 });
			reportList.Add(new reportLine() { Period = new DateTime(2018, 7, 2), StepID = 2, T = 18 });
			reportList.Add(new reportLine() { Period = new DateTime(2018, 7, 3), StepID = 2, T = 20 });
			reportList.Add(new reportLine() { Period = new DateTime(2018, 7, 5), StepID = 2, T = 24 });

			List<reportLine> reportList2 = reportList.FindAll(x => x.Period.Value.CompareTo(request.PeriodStart) >= 0 && x.Period.Value.CompareTo(request.PeriodEnd) <= 0 && request.StepIDs.Split(',').ToList().IndexOf(x.StepID.ToString()) != -1).OrderBy(x => x.Period).ToList();
			return reportList2;
		}

		[HttpPost]
		public List<reportColumn> PostStepCounter(requestCounter request)
		{
			List<reportColumn> reportColumns = new List<reportColumn>();

			reportColumns.Add(new reportColumn() { StepID = 1, StepDescription = "Camera 01" });
			reportColumns.Add(new reportColumn() { StepID = 2, StepDescription = "Camera 02" });

			return reportColumns.FindAll(x => request.StepIDs.Split(',').ToList().IndexOf(x.StepID.ToString()) != -1);
		}

	}
}
