﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Enliteon_Entity;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Web.Script.Serialization;
using System.Globalization;
using System.Threading;

namespace Enliteon_Site.Controllers {
	public class statsController : ApiController {

		[HttpPost]
		public List<reportLine> GetStatCounter([FromBody] requestCounter req) {
			List<reportLine> reportList = callAPI<List<reportLine>, requestCounter>("http://localhost:50123/api/Reports/PostStatCounter", req);
			return reportList.OrderBy(x=>x.Period).ToList();
		}

		[HttpPost]
		public List<reportColumn> GetStepCounter([FromBody] requestCounter req) {
			List<reportColumn> reportColumns = callAPI<List<reportColumn>, requestCounter> ("http://localhost:50123/api/Reports/PostStepCounter", req);
			return reportColumns;
		}




		#region callAPI
		#region Res callAPI<Res>(urlAPI)
		private Res callAPI<Res>(string urlAPI)
		{
			WebRequest request = WebRequest.Create(urlAPI);
			request.Method = "POST";
			request.ContentType = "application/json";
			request.ContentLength = 0;

			HttpWebResponse response = (HttpWebResponse)request.GetResponse();
			Stream dataStream = response.GetResponseStream();
			StreamReader reader = new StreamReader(dataStream);
			string responseFromServer = reader.ReadToEnd();
			reader.Close();
			dataStream.Close();
			response.Close();

			JavaScriptSerializer jss = new JavaScriptSerializer();
			Res result = jss.Deserialize<Res>(responseFromServer);
			return result;
		}
		#endregion
		#region Res callAPI<Res, Req>(urlAPI, Req DatiChiamata)
		private Res callAPI<Res, Req>(string urlAPI, Req DatiChiamata) {
			WebRequest request = WebRequest.Create(urlAPI);
			request.Method = "POST";
			request.ContentType = "application/json";

			DataContractJsonSerializer jsSer = new DataContractJsonSerializer(DatiChiamata.GetType());
			StreamWriter writer = new StreamWriter(request.GetRequestStream());
			JavaScriptSerializer jss = new JavaScriptSerializer();

			string sendData = jss.Serialize(DatiChiamata);
			writer.Write(sendData);
			writer.Close();

			HttpWebResponse response = (HttpWebResponse)request.GetResponse();
			Stream dataStream = response.GetResponseStream();
			StreamReader reader = new StreamReader(dataStream);
			string responseFromServer = reader.ReadToEnd();
			reader.Close();
			dataStream.Close();
			response.Close();

			Res result = jss.Deserialize<Res>(responseFromServer);
			return result;
		}
		#endregion
		#endregion



	}
}
