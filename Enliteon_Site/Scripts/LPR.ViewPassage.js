﻿var plate;
var currentServer;
var servers;

$(document).ready(function () {

    $.ajaxSetup({ cache: false }); //disabilita cache
    $('.paneOptions').hide(); //nasconde il pane di destra
    loadServers(); //carica i server e tutti gli altri pane
});

function fillHandleBars(id, data) {
    var source = $('#' + id + '-template').html();
    var template = Handlebars.compile(source);
    var html = template(data);
    $('#' + id + '-output').html(html);
}

function isFeatureAvailable(data,featureid)
{
    var found = false;

    for (var i = 0; i < data.length; i++)
        if (data[i].FeatureId == featureid)
            found = true;

    return found;
}

function loadByPassageId(passageid, loadlinked) {

    url = currentServer.Host + '/api/passages/getbypassageid';
    getJsonDataGet(url, 'id=' + passageid).done(function (data) {
        
        data.LoadCrops = loadCrops;
        data.PassageDateDisplay = moment(data.PassageDate).format('DD/MM/YY HH:mm:ss');
        data.Server = currentServer;
        data.Photo = encodeURIComponent(data.Photo);

        data.Adr = (data.ADR1 != null && data.ADR2 != null) ? data.ADR1 + '-' + data.ADR2 : '';
        data.Number = data.Adr.length > 1 ? data.Adr.substring(0, 1) : '';
        data.Description = (data.Description != null) ? data.Description : '';

        plate = data.Plate;

        
        //popola via Handlebars
        fillHandleBars('left', data);
        getJsonDataGet('/api/parameters/getcamfeatures', 'camid=' + data.CamID).done(function (features) {

            //IR = 3 
            if (isFeatureAvailable(features, 3) || isFeatureAvailable(features, 8))
                fillHandleBars('image-ir', data);

            //CTX = 2
            if (isFeatureAvailable(features, 2) || isFeatureAvailable(features, 9))
                fillHandleBars('image-ctx', data);

        });

        
        fillHandleBars('server', servers);

        $('.link-server-' + currentServer.ServerId).addClass('selected');
        $('.link-server').unbind('click');
        $('.link-server').click(function () {

            $('.link-server').removeClass('selected');
            $(this).addClass('selected');

            currentServer = findServerById($(this).data('serverid'), servers);

            if (currentServer == null) //error: server non trovato
                return;

            loadLinkedPassages(plate);


        });

        if (loadlinked != null && loadlinked)
            loadLinkedPassages(plate);

    });
}

function loadServers() {
    //recupera l'elenco deio server
    var url = '/api/servers/getservers';
    getJsonDataGet(url, '').done(function (data) {

        servers = data;

        if (servers == null || servers.length == 0) //error: server non presenti
            return;

        var sid = getUrlParameter('SID');
        var passageid = getUrlParameter('ID');

        if (sid == null) //error: sid non specificato
            return;

        var server = findServerById(sid, servers);

        if (server == null) //error: server non trovato
            return;

        currentServer = server;

        loadByPassageId(passageid, true);

    });
}

function loadLinkedPassages(plate) {
    url = currentServer.Host + '/api/passages/getotherpassages';
    getJsonDataGet(url, 'Plate=' + plate).done(function (data) {
       
        jQuery.each(data, function () {

            //$(this)[0].PassageDateDisplay = moment($(this)[0].PassageDate).format('DD/MM/YY') + ' ' + moment($(this)[0].PassageDate).format('HH:mm:ss')
            $(this)[0].Server = currentServer;
        });

        var passageid = getUrlParameter('ID');

        //popola via Handlebars
        fillHandleBars('passages', data);

        $('.row').removeClass('selected');
        $('[data-passageid=' + passageid + ']').addClass('selected');

        $('.link-load').click(function () {

            passageid = $(this).data('cellpassageid');
            $('.row').removeClass('selected');
            $('[data-passageid=' + passageid + ']').addClass('selected');
            loadByPassageId(passageid, false);
        });

    });
}

function findServerById(id, servers) {
    for (var i = 0; i < servers.length; i++)
        if (servers[i].ServerId == id)
            return servers[i];

    return null;
}

function getUrlParameter(sParam) {
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
};

//costruisce la stringa parametri da passare dopo il ? nella URL
function getParameters(isRemote) {
    var province = '%';
    var adrfilter = '';
    var pagesize = 500;
    var now = moment();
    var fromDate = now.add(-20, 'y').toJSON();
    var toDate = now.add(20, 'y').toJSON();

    //crea un array dei parametri
    var par = [];
    par.push({ name: "Gates", value: '' });
    par.push({ name: "Plate", value: plate });
    par.push({ name: "Province", value: province });
    par.push({ name: "Rows", value: pagesize });
    par.push({ name: "From", value: fromDate });
    par.push({ name: "To", value: toDate });
    par.push({ name: "Sort", value: 'DESC' });
    par.push({ name: "Type", value: '' });
    par.push({ name: "AdrFilter", value: adrfilter });
    par.push({ name: "Blacklists", value: '' });
    par.push({ name: "Remote", value: isRemote });
    par.push({ name: "Last", value: '' });

    //genera la stringa dei parametri
    var url = '';
    for (var i = 0; i < par.length; i++) {
        if (url != '') url += '&';
        url += par[i].name + '=' + encodeURIComponent(par[i].value);
    }

    return url;
}