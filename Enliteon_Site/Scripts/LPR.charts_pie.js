﻿
//Draw a pie chart using specific provider (Google, AM)
function drawPieChart(data, chart_title, chart_div, provider,otherlabel) {

    if (provider === 'Google') {

        $('.Google').show();
        var formatted_data = google.visualization.arrayToDataTable(data);
        var formatter = new google.visualization.NumberFormat({ groupingSymbol: '.', fractionDigits: 0 });
        formatter.format(formatted_data, 1);

        var options = {
            title: chart_title,
            chartArea: { width: 600, top: 40, height: 200, left: 0 },
            titleTextStyle: { color: '#404040', fontSize: 16 },
            is3D: true,
            pieResidueSliceLabel: otherlabel,
            pieSliceTextStyle: { color: '#ffffff', fontSize: 10 },
            legend: { position: 'right', textStyle: { color: '#404040', fontSize: 10 } }
        };

        var chart2 = new google.visualization.PieChart(document.getElementById(chart_div));
        chart2.draw(formatted_data, options);

    } else if (provider === 'AM') {


     
        $('.AM').show();
         
        // PIE CHART
        var chart = new AmCharts.AmPieChart();
        //chart.dataProvider = getDataAm('Euro', fromdate, todate);
        chart.dataProvider = data;
        chart.titleField = "Label";
        chart.valueField = "Value";
        //chart.outlineColor = "#FFFFFF";
        //chart.outlineAlpha = 0.8;
        chart.outlineThickness = 1;
        chart.innerRadius = "50%";
        chart.groupPercent = 1;
        chart.groupedTitle = otherlabel;
        chart.colors = ["#01b8aa", "#fd625e", "#f2c811", "#4e6466", "#8ad4eb", "#fe9666", "#a66999", "#3599b8", "#dfbfbf", "#5f6b6d", "#f4d25a", "#7f898a", "#a4ddee", "#fdab89", "#b687ac", "#28738a", "#b59525", "#0f5c55", "#1c2325", "#7d3231", "#bd7150", "#1b4d5c"];
        chart.percentFormatter = {
            precision: 1, decimalSeparator: ",", thousandsSeparator: "."
        };
        chart.numberFormatter = {
            precision: 0, decimalSeparator: ",", thousandsSeparator: "."
        };
        chart.balloonText = "[[title]]<br><span style='font-size:12px'><b>[[value]]</b> ([[percents]]%)</span>";
        //chart.labelText = "[[title]] | [[value]] | [[percents]]%";
        chart.labelText = "[[title]] [[percents]]%";
        chart.fontSize = 12;
        chart.color = $('.chart-text-color').css('color');
        // this makes the chart 3D
        chart.depth3D = 0;
        chart.angle = 0;
        chart.radius = 130;
        //chart.marginTop = 50;
        chart.labelTickColor = '#FFFFFF';
        chart.labelTickAlpha = 0.3;


        var legend = new AmCharts.AmLegend();
        legend.labelText = "[[title]]";
        legend.position = "right";
        legend.fontSize = 9;
        legend.numberFormatter = {
            precision: 0, decimalSeparator: ",", thousandsSeparator: "."
        };
        //legend.borderColor = "#dedede";
        //legend.borderAlpha = 1;
        //legend.horizontalGap = 15;
        //legend.verticalGap = 15;
        legend.markerBorderColor = "gray";
        legend.markerType = "circle";
        legend.markerSize = 10;
        legend.fontSize = 9;
        legend.color = $('.chart-legend-color').css('color');
        //legend.backgroundAlpha = 1;
        //legend.backgroundColor = "whitesmoke";
        legend.valueText = "[[value]]";
        //chart.addLegend(legend);

        //chart.addTitle(chart_title, 16);

        // WRITE
      
     
        chart.write(chart_div);
    }
}