﻿function loadSelectedGates() {
    var gates = '';

    $('.Gates input:checked').each(function () {

        if (gates !== '')
            gates += ',';

        gates += $(this).val();


    });
    return gates;
}

function loadSelectedEvents() {
    var events = '';

    $('.Events input:checked').each(function () {

        if (events !== '')
            events += ',';

        events += $(this).val();


    });
    return events;
}


function showDialog(message1, message2) {
    //dialog message init
    $(function () {

        if ($('#dialog-message').length === 0)
            return;

        $('#dialog-message').find('.message1').html(message1);
        if (message2 !== '')
            $('#dialog-message').find('.message2').html(message2);

        $("#dialog-message").dialog({
            modal: true,
            width: 650,
            buttons: {
                Ok: function () {
                    $(this).dialog("close");
                }
            }
        });
    });
}
 

function getGateName(data, id) {
	var ret = 'N/D';
	for (var i = 0; i < data.length; i++) {
		if ((data[i].StepID === id) || (data[i].StepID === id)) {
			ret = data[i].StepDescription;
		}
	}
	return ret;
}
//Fill data table, only Google supported
function drawTable(data, chart_div) {

    var table = new google.visualization.Table(document.getElementById(chart_div));
    

    var formatted_data = google.visualization.arrayToDataTable(data);
    //data = new google.visualization.DataTable(jsonData);

    var formatter = new google.visualization.NumberFormat({ groupingSymbol: '.', fractionDigits: 0 });
    formatter.format(formatted_data, 1);
     
    table.draw(formatted_data, { showRowNumber: false });
}

//Fill data table, only Google supported
function drawTableStatistics(data, chart_div) {

    var table = new google.visualization.Table(document.getElementById(chart_div));
    var formatted_data = new google.visualization.DataTable(jsonData);

    var formatter = new google.visualization.NumberFormat({ groupingSymbol: '.', fractionDigits: 0 });
    formatter.format(formatted_data, 1);

    table.draw(formatted_data, { showRowNumber: false });
}

//Calls JSON Web Service to Get GateInfo Data (GET)
function getJsonDataGet(url, request, timeout) {
    
    //default timeout
    if (timeout === null)
        timeout = 5000;
    return $.ajax({
        type: "GET",
        cache: false,
        url: url + '?' + request,
        dataType: "json",
        timeout: timeout
    });

}

//Calls JSON Web Service to Get GateInfo Data (POST)
function getJsonData(url, request) {
    return $.ajax({
        type: "POST",
        url: url,
        dataType: "json",
        data: request
    });
}
