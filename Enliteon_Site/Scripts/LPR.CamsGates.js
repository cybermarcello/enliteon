﻿function ConfirmOnDelete(item) {
    if (confirm("Attenzione !\n\nEliminando questo varco saranno eliminati anche i relativi passaggi e le statistiche") == true)
        return true;
    else
        return false;
}

function showPane(obj) {
    $(obj).slideDown();
}

function initCam() {
    showPane('.addCam');
}
function initMap() {

    
    $('.paneOptions').hide();
    var green = L.icon({ iconUrl: '/img/pin-green.png', shadowUrl: '/img/pin-shadow.png', iconSize: [25, 41], iconAnchor: [12, 41], shadowSize: [41, 41], shadowAnchor: [12, 41], popupAnchor: [4, -50] });

    var gpslat = 45.472343;
    var gpslong = 9.184940;
    var zoom_small = 8;
    var zoom_big = 15;

    if (map != null)
        map.remove();

    var latValue = $('.lat').val().replace(',', '.');
    var lngValue = $('.lng').val().replace(',', '.');

    if (latValue != '' && lngValue != '') {
        $('#mymap').slideDown('fast', function () {

            map = L.map('mymap', { closePopupOnClick: false }).setView([latValue, lngValue], zoom_big).on('dragstart', function () { $('embed').hide(); }).on('dragend', function () { $('embed').show(); });
            marker = L.marker([latValue, lngValue], { icon: green }).addTo(map);
            // add an OpenStreetMap tile layer
            L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(map);

            map.on('click', onMapClick);

        });
        showPane('.addGate');
    }
    $('.lat,.lng').focusin(function () {
        $('#mymap').slideDown('fast', function () {

            if (latValue == '' && lngValue == '') {
                latValue = gpslat;
                lngValue = gpslong;
                zoom_big = zoom_small;
            }

            map = L.map('mymap', { closePopupOnClick: false }).setView([latValue, lngValue], zoom_big).on('dragstart', function () { $('embed').hide(); }).on('dragend', function () { $('embed').show(); });
            marker = L.marker([latValue, lngValue], { icon: green }).addTo(map);


            // add an OpenStreetMap tile layer
            L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(map);

            map.on('click', onMapClick);
        });
    });

    function onMapClick(e) {
        
        map.setView([e.latlng.lat, e.latlng.lng]);
        marker.setLatLng(e.latlng);
        $('.lat').val(e.latlng.lat.toString().replace('.', ','));
        $('.lng').val(e.latlng.lng.toString().replace('.', ','));
    }

   
   

}