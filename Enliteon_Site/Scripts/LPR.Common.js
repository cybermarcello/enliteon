﻿function bindTemplate(obj, data)
{
    var source = $(obj + '-template').html();
    var template = Handlebars.compile(source);
    var html = template(data);
    $(obj + '-output').html(html);
}

function getTemplate(obj, data) {
    var source = $(obj + '-template').html();
    var template = Handlebars.compile(source); 
    return template(data);
}



function showTicket(id)
{
    $('#Container').css('filter','blur(5px)');
    $('.mask').show();
    $('.ticket-window').show();
    $('.ticket-note-button').data('ticketid', id);

   

    var source = $("#note-template").html();
    var template = Handlebars.compile(source);
    var html = template();
    $('#note-output').html(html);
    $('.ticket-note-body').val('');
    $('.reasons').val(-1);
    $('.ticket-error').hide();
    $("#ticketfull-output").html('');

    var audio = document.getElementById('ticketnotify');
    var count = $('#errorPane .ticket').length;
    if (count==0)
    audio.pause();
}

function showTicketFull(id) {
    $('#Container').css('filter', 'blur(5px)');
    $('.mask').show();
    $('.ticket-window').show();
    $('.ticket-note-button').data('ticketid', id);

    $("#ticketfull-output").html('');
    $("#note-output").html('');
    var req1 = getJsonDataGet('/api/tickets/GetNotes', 'EventId=' + id);
    var req2 = getJsonDataGet('/api/tickets/GetTicketById', 'UserId=' + userid + '&TicketId=' + id);

    $.when(req1, req2).done(function (notes,ticket) {
        


        var source = $("#ticketfull-template").html();
        var template = Handlebars.compile(source);
        var html = template(ticket[0]);        
        $('#ticketfull-output').html(html);
        

       var source = $("#note-template").html();
        var template = Handlebars.compile(source);
        var html = template(notes[0]);
        $('#note-output').html(html);

    }).fail(function (jqxhr, textStatus, error) {
                  
        var webapierror = $.parseJSON(jqxhr.responseText);
        var err = webapierror.Message;
        alert(err);
               });

 
    $('.ticket-note-body').val('');
    $('.reasons').val(-1);
    $('.ticket-error').hide();

    var audio = document.getElementById('ticketnotify');
    var count = $('#errorPane .ticket').length;
    if (count == 0)
        audio.pause();
}

//Funzione richiamata dalla dialog box con tutti i preset di ricerche con pattern
//riempie il box targa con il preset selezionato
function fill(t) {

    $('#plate').val(t);
    $("#dialog-message").dialog("close");
    loadData(0);
}

function closeTicket() {
    if ($('.reasons').val() == '-1')
    {
        $('.ticket-error').fadeIn();
        return;
    }

    var reasonid = $('.reasons').val();
    var eventid = $('.ticket-note-button').data('ticketid');
     
    getJsonDataGet('/api/tickets/CloseTicket', 'UserId=' + userid + '&EventId=' + eventid+'&ReasonId='+reasonid).done(function (data) {

        $('#Container').css('filter', '');
        $('.mask').hide();
        $('.ticket-window').hide();
        if (typeof loadTickets === "function")
            loadTickets();

    }).fail(function (jqxhr, textStatus, error) {
        var webapierror = $.parseJSON(jqxhr.responseText);
        var err = webapierror.Message;
        alert(err);
       
    });
        

}

function hideTicket() {
    $('#Container').css('filter', '');
    $('.mask').hide();
    $('.ticket-window').hide();
    
    if (typeof loadTickets === "function")
        loadTickets();
}

function loadNotes(ticketId)
{
     
    getJsonDataGet('/api/tickets/GetNotes', 'EventId=' + ticketId).done(function (data) {

        var source = $("#note-template").html();
        var template = Handlebars.compile(source);
        var html = template(data);
        $('#note-output').html(html);

    }).fail(function (jqxhr, textStatus, error) {
        var webapierror = $.parseJSON(jqxhr.responseText);
        var err = webapierror.Message;
        alert(err);
    });
}
function createNote(t)
{
    var ticketBody = $('.ticket-note-body').val();
    var ticketId = $(t).data('ticketid');
    
  
    getJsonDataGet('/api/tickets/CreateNote', 'UserId=' + userid + '&TicketId=' + ticketId + '&Body=' + encodeURIComponent(ticketBody)).done(function (data) {
        
        $('.ticket-note-body').val('');
        loadNotes(ticketId);

    }).fail(function (jqxhr, textStatus, error) {
        var webapierror = $.parseJSON(jqxhr.responseText);
        var err = webapierror.Message;
        alert(err);
        //showAlertBox('errorbar', 'ERROR', err);
    });
}

function createTicket(passageid,eventid)
{
    getJsonDataGet('/api/tickets/CreateTicket', 'UserId=' + userid + '&EventId=' + eventid).done(function (data) {
        showTicket(eventid);
    }).fail(function (jqxhr, textStatus, error) {
        var webapierror = $.parseJSON(jqxhr.responseText);
        var err = webapierror.Message;
        alert(err);
        //showAlertBox('errorbar', 'ERROR', err);
    });
}
function showEvent(obj) {

    obj.PassageDateFormatted = moment(obj.PassageDate).format('DD/MM/YY HH:mm:ss');

    if ($('#errorPane .ticket_' + obj.PassageId).length>0)
        return;

    var padbottom = 10;
    var height = 0;
    var count = $('#errorPane .ticket').length;
    
    if (count > 0)
        height = parseInt($('#errorPane .ticket').first().css('height'));
    else {
        var times = 10000;
        var audio = document.getElementById('ticketnotify');
        audio.addEventListener('ended', function () { audio.play(); times = times - 1; if (times == 0) audio.pause(); });
        audio.play();
       
    }

    var top = count * (padbottom + height) + 50;
     
    var source = $("#ticket-template").html();
    var template = Handlebars.compile(source);
    var html = template(obj);
  


    var ticket = $(html);
    ticket.css('top', top);

    $('#errorPane').append(ticket);
    $(ticket).animate(
           { opacity: 1, right: 15, duration: 'fast' }
           );

}
function showAlertBox(object, type, message) {

    var object = '.' + object;
   
    if (object == '.errorbar') {

        var coll = $(object);
        var found = false;
        var foundobj;
        coll.each(function (index) {
            if ($(this).find('span').html() == '')
            {
                if (!found) {
                    object = coll[index];
                    found = true;
                }

            }
        });

        if (found == false)
        {
            var last = coll.last();
            var clone = $(coll[0]).clone();
            clone.css('top', parseInt(last.css('top')) + 55);
            clone.css('right',-250);
            $('#errorPane').append(clone);
            object=clone;
            
        }
        
        $(object).removeClass('BarError').removeClass('BarOk');
        $(object).find('.ErrorMessage').html('<span class="ErrorMessage">' + message + '</span>');


        if (type == 'ERROR') {
            $(object).addClass('BarError');
            $(object).find('img').attr('src', '/img/error.png');
        }

        if (type == 'OK') {
            $(object).addClass('BarOk');
            $(object).find('img').attr('src', '/img/yes.png');
        }

        $(object).show();
        $(object).animate(
            { opacity: 1, right: 20,duration:'fast' }
            ).delay(1750).animate(
            { opacity: 0, right: -250 }, 250, function () {
                $(object).find('span').html('');
            }
            );
        

    }
    else
    {
        $(object).removeClass('AlertError').removeClass('AlertOk');
        $(object).find('.ErrorMessage').html('<span class="ErrorMessage">' + message + '</span>');


        if (type == 'ERROR') {
            $(object).addClass('AlertError');
            $(object).find('img').attr('src', '/img/error.png');
        }

        if (type == 'OK') {
            $(object).addClass('AlertOk');
            $(object).find('img').attr('src', '/img/yes.png');
        }

        $(object).fadeIn().delay(2500).fadeOut();
    }

    return;
}

$(document).ready(function () {


    resize();
    $('.toggle').prop('src', '/Content/img/menu_up.gif');
    $('.toggle').click(function () {

        var path = 'nav > ul.menuPane_' + $(this).attr('data-index') + ' > .second';

				$(this).attr('src') == '/Content/img/menu_up.gif' ? $(path).slideUp() : $(path).slideDown();
				$(this).attr('src') == '/Content/img/menu_up.gif' ? $(this).attr('src', '/Content/img/menu_down.gif') : $(this).attr('src', '/Content/img/menu_up.gif');
				$(this).attr('src') == '/Content/img/menu_up.gif' ? $('.menuPane_' + $(this).attr('data-index')).attr('data-status', 'open') : $('.menuPane_' + $(this).attr('data-index')).attr('data-status', 'closed');
				$(this).attr('src') == '/Content/img/menu_up.gif' ? $('.menuPane_' + $(this).attr('data-index')).attr('data-index', $(this).attr('data-index')) : $('.menuPane_' + $(this).attr('data-index')).attr('data-index', $(this).attr('data-index'));

        saveMenu();

    });

    var list = readCookie('RoomsList');
    if ((list != null) && (list != '')) {
        var arr = list.split('|');
        var index = 0;



        $('.first').each(function (index) {

            var path = 'nav > ul.menuPane_' + arr[index] + ' > .second';
            $(path).hide();
            $('.menuPane_' + arr[index]).attr('data-status', 'closed');
            $('.toggle[data-index=\'' + arr[index] + '\']').attr('src', '/img/menu_down.gif');
            index++;
        });


    }


    $('.toggle').fadeIn();

});


function resize() {
    $('#mycell').css('height', $(window).height() - 43);
    $('#mycell').css('width', $(window).width() - 225);

    if ($('.ChartPie') != null)
        $('.ChartPie').css('width', $(window).width() - 225 - 250);

    $('#map_canvas').css('height', $(window).height() - 43);
    $('#map_canvas').css('width', $(window).width() - 225);
    return;
}
$(window).resize(function () {

    resize();


});

function saveMenu() {


    var result = '';
    $('.first[data-status=\'closed\']').each(function (index) {

        if (result != '')
            result = result + '|';


        result = result + $(this).attr('data-index');
    });

    createCookie('RoomsList', result, 365);

    return;
}
function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}


function openDashboard() {
    window.open("/admin/console.aspx", "mywindow", "status=0,toolbar=0");
    return;
}


//Hide and show dynamically Left Pane Menu
function closeMenu() {

    $('.LeftMenu').css('display', 'block');


    if (parseInt($('div.LeftCell').css('width')) <= 1) {
        $('.LeftMenu').animate({
            width: 220,
            opacity: 1
        }, 500, function () {

        });


    }
    else {

        $('.LeftMenu').css('overflow', 'hidden');
        $('.LeftMenu').animate({
            width: 0,
            opacity: 0
        }, 500, function () {
            $('.LeftMenu').css('display', 'none');
        });


    }


    return;
}