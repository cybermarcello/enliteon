﻿function drawBarChart(data, chart_title, chart_div, provider,charttype,gates_data,category_title,number_title) {
      

    


        var gatesDetails = gates_data;

        var chart_am;
        var from = $('.txtFrom').val();
        var to = $('.txtTo').val();
        var gates = loadSelectedGates();
        var aggregatedChart = false;

        //if no gates are selected then we need to draw aggregated result with 1 data-point Series
        if (gates == '') {
            aggregatedChart = true;
            gates = '1';
        }

        //default chart type
        var type;
        var smoothed = true;

        if ($('input[name=type]:checked').val() != null)
            type = $('input[name=type]:checked').val();

         
        if ($('.smoothed').length!=0)
            smoothed = $('.smoothed').prop('checked');

        var stacked = $('.stacked').prop('checked');
        var dimension = $('.dimension').prop('checked');

        //stacked = false;
        type = charttype;
        smoothed = false;
     
        var gates_split = gates.split(',');


        chartData = data;
        // SERIAL CHART
        chart_am = new AmCharts.AmSerialChart();
        //chart_am.theme = AmCharts.themes.light;
        chart_am.dataProvider = chartData;
        chart_am.categoryField = "date";
        chart_am.startDuration = 1;
        chart_am.fontSize = 9;
        //AmCharts.theme = AmCharts.themes.black;  
        //chart_am.backgroundColor = '#404040';
        //chart_am.backgroundAlpha = 1;
        chart_am.color = $('.chart-text-color').css('color');

        chart_am.plotAreaBorderColor = "#DADADA";
        chart_am.plotAreaBorderAlpha = 1;
       
        chart_am.percentFormatter = {
            precision: 1, decimalSeparator: ",", thousandsSeparator: "."
        };
        chart_am.numberFormatter = {
            precision: 0, decimalSeparator: ",", thousandsSeparator: "."
        };

      
     
        //gap between column of the same serie
        chart_am.columnSpacing = 0;
         
        // this single line makes the chart a bar chart
        chart_am.rotate = false;

        
        //only for 3D
        if (dimension) {
            chart_am.depth3D = 25;
            chart_am.angle = 30;
        }

        // AXES
        // Category
        var categoryAxis = chart_am.categoryAxis;
        categoryAxis.gridPosition = "start";
        categoryAxis.gridAlpha = 0.1;
        categoryAxis.axisAlpha = 0;
        categoryAxis.gridColor = '#c0c0c0';
        

        // Value
        var valueAxis = new AmCharts.ValueAxis();
        valueAxis.axisAlpha = 0;
        valueAxis.gridAlpha = 0.2;
        valueAxis.gridColor = '#c0c0c0';
         

        //if (chart_title=='speed')
        //    valueAxis.maximum = 120;

        valueAxis.position = "left";
        chart_am.addValueAxis(valueAxis);

        if (type==='bars')
        chart_am.rotate = true;

        // GRAPHS
        // first graph
        for (var i = 0; i < gates_split.length; i++) {
            var graph1 = new AmCharts.AmGraph();

            console.log(gatesDetails);
            console.log(gates_split[i]);
            graph1.title = getGateName(gatesDetails, parseInt(gates_split[i]));
            console.log(graph1.title);
            if (aggregatedChart)
                graph1.title = 'Totale varchi';

            if (category_title == null)
                category_title = '';
            if (number_title == null)
                number_title = '';

            graph1.valueField = gates_split[i];
            graph1.balloonText = "<div style='padding:4px;line-height:18px'><span style='font-size:9pt'><b>[[title]]</b></span><br /><span style='font-size:8pt'><b>" + category_title + ":</b> [[category]] - <b>" + number_title + ":</b> [[value]]</span></div>";
            graph1.lineAlpha = 1;
              
            if ((type === 'columns') || (type==='bars')) {
              
                graph1.type = "column";
                graph1.fillAlphas = 0.9;
            
                chart_am.sequencedAnimation = true;
            }
            else if (type == 'line') {
                graph1.type = "line";
                graph1.fillAlphas = 0;
                graph1.bullet = "round";
                graph1.bulletSize = 7;
                graph1.lineThickness = 2;
                chart_am.sequencedAnimation = false;
            }
            else if (type == 'area') {
                graph1.type = "line";
                graph1.fillAlphas = 0.75;
                chart_am.sequencedAnimation = false;
                //graph1.bullet = "round";
                //graph1.lineThickness = 2;
            }

            if (stacked)
                valueAxis.stackType = "regular";

            if (smoothed)
                graph1.type = "smoothedLine";


            chart_am.addGraph(graph1);
        }




        // LEGEND
        var legend = new AmCharts.AmLegend();
        legend.labelText = "[[title]]";
        legend.position = "bottom";
        legend.fontSize = 9;
        legend.markerSize = 10;
        legend.markerType = 'round';
        legend.verticalGap = 1;
        legend.color = $('.chart-legend-color').css('color');
        legend.numberFormatter = {
            precision: 0, decimalSeparator: ",", thousandsSeparator: "."
        };


        //chart_am.addLegend(legend);


        // WRITE
        chart_am.write(chart_div);
 


    return;
}