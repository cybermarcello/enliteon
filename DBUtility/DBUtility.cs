﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

namespace EnliteonLibraries.Common {
	public class DbUtils {
		private string ConnectionString;
		private static LogLocation location = LogLocation.Disk | LogLocation.Console;

		#region Costruttore della classe
		public DbUtils() {
			string keyname = "SQLConnection";
			if (CheckDatabaseString("SQLConnection")) {
				this.ConnectionString = ConfigurationManager.ConnectionStrings[keyname].ConnectionString;
			}
		}
		public DbUtils(string connectionString) {
			this.ConnectionString = connectionString;
		}
		#endregion

		#region CheckDatabaseString
		public bool CheckDatabaseString(string keyname) {
			bool ret = false;

			try {
				if (ConfigurationManager.ConnectionStrings[keyname] == null) {
					ret = false;
					string message = $"Connection string {keyname} not defined, please check .exe.config or machine.config file";
					throw new Exception(message);
				} else {
					ret = true;
					this.ConnectionString = ConfigurationManager.ConnectionStrings[keyname].ConnectionString;
					Logger.Write("[OK] Connection String has been found!", location);
				}
			} catch (Exception ex) {
				string message = $"[ERROR] {ex.Message}";
				Logger.Write(message, location);
				throw ex;
			}

			return ret;
		}
		#endregion
		#region CheckDatabaseAvailability
		public bool CheckDatabaseAvailability() {
			bool ret = false;
			try {
				string strsql = this.ConnectionString;
				using (SqlConnection conn = new SqlConnection(strsql)) {

					conn.Open();
					conn.Close();
					ret = true;
				}

				Logger.Write("[OK] Database up and running!", location);
			} catch (SqlException ex) {
				Logger.Write("[ERROR] Database connection not available, please check!", location);
				string message = $"[ERROR] {ex.Message}";
				Logger.Write(message, location);
				throw ex;
			}

			return ret;
		}
		#endregion

		#region PurgeDB
		public void PurgeDB(string UUID) {
			try {
				using (SqlConnection conn = new SqlConnection(ConnectionString)) {

					conn.Open();

					SqlCommand cmd = new SqlCommand("PuliziaGiornaliera", conn);
					cmd.CommandTimeout = 120;
					cmd.CommandType = CommandType.StoredProcedure;

					cmd.Parameters.AddWithValue("@uuid", UUID);

					cmd.ExecuteNonQuery();

					conn.Close();
				}
			} catch (SqlException ex) {
				Logger.Write(String.Format("[SQL Exception] {0}", ex.Message), location);
			} catch (Exception ex) {
				Logger.Write(String.Format("[Exception] {0}", ex.Message), location);
			}
		}
		#endregion
		#region getCameraID
		public long getCameraID(string UUID) {
			try {
				long idCamera = -1;
				using (SqlConnection conn = new SqlConnection(ConnectionString)) {

					conn.Open();

					SqlCommand cmd = new SqlCommand("getCameraInfo", conn);
					cmd.CommandTimeout = 120;
					cmd.CommandType = CommandType.StoredProcedure;

					cmd.Parameters.AddWithValue("@uuid", UUID);

					SqlDataReader rdr = cmd.ExecuteReader();
					if (rdr.Read()) {
						idCamera = rdr.GetInt32(0);
					}

					conn.Close();
				}
				return idCamera;
			} catch (SqlException ex) {
				Logger.Write(String.Format("[SQL Exception] {0}", ex.Message), location);
			} catch (Exception ex) {
				Logger.Write(String.Format("[Exception] {0}", ex.Message), location);
			}
			return -1;
		}
		#endregion
		#region CounterInserisci
		public void CounterInserisci(long IdCamera, DateTime DataEvento, long TotIn, long TotOut) {
			try {
				using (SqlConnection conn = new SqlConnection(ConnectionString)) {

					conn.Open();

					SqlCommand cmd = new SqlCommand("Counter_Inserisci", conn);
					cmd.CommandTimeout = 120;
					cmd.CommandType = CommandType.StoredProcedure;

					cmd.Parameters.AddWithValue("@IdCamera", IdCamera);
					cmd.Parameters.AddWithValue("@Timestamp", DataEvento);
					cmd.Parameters.AddWithValue("@TotIn", TotIn);
					cmd.Parameters.AddWithValue("@TotOut", TotOut);

					cmd.ExecuteNonQuery();

					conn.Close();
				}
			} catch (SqlException ex) {
				Logger.Write(String.Format("[SQL Exception] {0}", ex.Message), location);
			} catch (Exception ex) {
				Logger.Write(String.Format("[Exception] {0}", ex.Message), location);
			}
		}
		#endregion
		#region PolygonInserisci
		public void PolygonInserisci(long IdCamera, DateTime DataEvento, long Contatore, List<string> TrackIds) {
			try {
				using (SqlConnection conn = new SqlConnection(ConnectionString)) {

					conn.Open();

					foreach (string TrackId in TrackIds) {
						SqlCommand cmd = new SqlCommand("Polygon_Inserisci", conn);
						cmd.CommandTimeout = 120;
						cmd.CommandType = CommandType.StoredProcedure;

						cmd.Parameters.AddWithValue("@IdCamera", IdCamera);
						cmd.Parameters.AddWithValue("@Timestamp", DataEvento);
						cmd.Parameters.AddWithValue("@TrackId", System.Convert.ToInt32(TrackId));

						cmd.ExecuteNonQuery();
					}

					conn.Close();
				}
			} catch (SqlException ex) {
				Logger.Write(String.Format("[SQL Exception] {0}", ex.Message), location);
			} catch (Exception ex) {
				Logger.Write(String.Format("[Exception] {0}", ex.Message), location);
			}
		}
		#endregion
		#region TransitionsInserisci
		public void TransitionsInserisci(long IdCamera, DateTime DataEvento, long TrackId, long In, long Out) {
			try {
				using (SqlConnection conn = new SqlConnection(ConnectionString)) {

					conn.Open();

					SqlCommand cmd = new SqlCommand("Transitions_Inserisci", conn);
					cmd.CommandTimeout = 120;
					cmd.CommandType = CommandType.StoredProcedure;

					cmd.Parameters.AddWithValue("@IdCamera", IdCamera);
					cmd.Parameters.AddWithValue("@Timestamp", DataEvento);
					cmd.Parameters.AddWithValue("@TrackId", TrackId);
					cmd.Parameters.AddWithValue("@In", In);
					cmd.Parameters.AddWithValue("@Out", Out);

					cmd.ExecuteNonQuery();

					conn.Close();
				}
			} catch (SqlException ex) {
				Logger.Write(String.Format("[SQL Exception] {0}", ex.Message), location);
			} catch (Exception ex) {
				Logger.Write(String.Format("[Exception] {0}", ex.Message), location);
			}
		}
		#endregion
		#region TracksInserisci
		public void TracksInserisci(long IdCamera, DateTime DataEvento, long TrackId, long PosX, long PosY, long Height) {
			try {
				using (SqlConnection conn = new SqlConnection(ConnectionString)) {

					conn.Open();

					SqlCommand cmd = new SqlCommand("Tracks_Inserisci", conn);
					cmd.CommandTimeout = 120;
					cmd.CommandType = CommandType.StoredProcedure;

					cmd.Parameters.AddWithValue("@IdCamera", IdCamera);
					cmd.Parameters.AddWithValue("@Timestamp", DataEvento);
					cmd.Parameters.AddWithValue("@TrackId", TrackId);
					cmd.Parameters.AddWithValue("@PosX", PosX);
					cmd.Parameters.AddWithValue("@PosY", PosY);
					cmd.Parameters.AddWithValue("@Height", Height);

					cmd.ExecuteNonQuery();

					conn.Close();
				}
			} catch (SqlException ex) {
				Logger.Write(String.Format("[SQL Exception] {0}", ex.Message), location);
			} catch (Exception ex) {
				Logger.Write(String.Format("[Exception] {0}", ex.Message), location);
			}
		}
		#endregion

	}
}
