﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

namespace EnliteonLibraries.Common{
	public class Logger {
		private static readonly Object _syncObject = new Object();

		#region WriteToEventViewer
		private static void WriteToEventViewer(string message) {
			try {
				string sSource;
				string sLog;
				string sBody;

				sSource = "Enliteon Notifier";
				sLog = "Application";
				sBody = message;

				if (!EventLog.SourceExists(sSource))
					EventLog.CreateEventSource(sSource, sLog);


				EventLog.WriteEntry(sSource, sBody, EventLogEntryType.Information, 234);
			} catch {
			}
		}
		#endregion
		#region WriteToDisk
		private static void WriteToDisk(string message, string folder) {
			try {
				lock (_syncObject) {
					Directory.SetCurrentDirectory(AppDomain.CurrentDomain.BaseDirectory);

					string filename = string.Empty;

					//se la cartella non è vuota
					if (folder != String.Empty) {
						string curdir = AppDomain.CurrentDomain.BaseDirectory;
						string newpath = String.Format("{0}{1}", curdir, folder);

						//crea la cartella se non esiste
						if (!Directory.Exists(newpath))
							Directory.CreateDirectory(newpath);

						filename = String.Format("{0}\\Log_{1}.txt", newpath, DateTime.Now.ToString("yyyyMMdd"));
					} else
						filename = String.Format("Log_{0}.txt", DateTime.Now.ToString("yyyyMMdd"));

					FlushToDisk(filename, message);

					//esegue le operazioni di purge che possono essere intensive solo dalle 00:00 alle 00:30
					if (DateTime.Now.Hour == 0 && DateTime.Now.Minute <= 30) {
						//recupera i file nella cartella corrente (vecchi files premodifica agente)
						string[] files1 = Directory.GetFiles(Directory.GetCurrentDirectory(), "Log_*.txt");

						//recupera i nuovi files cartella log
						string[] files2 = Directory.GetFiles(String.Format("{0}\\{1}", Directory.GetCurrentDirectory(), folder), "Log_*.txt");

						//effettua un merge di tutti i file
						var lst = new List<string>();
						lst.AddRange(files1);
						lst.AddRange(files2);

						//numero di giorni di history da mantenere
						int historydays = 30;

						//purge dei logs
						foreach (var item in lst) {
							try {

								DateTime modification = File.GetLastWriteTime(item);
								DateTime maxdate = DateTime.Now.AddDays(-historydays);
								if (modification < maxdate) {
									message = String.Format("[INFO] Purge filename {0}", Path.GetFileName(item));
									FlushToDisk(filename, message);

									//eliminazione file
									try {
										File.Delete(item);
									} catch (Exception ex) {
										message = String.Format("[ERROR] Error purging {0}: {1}", Path.GetFileName(item), ex.Message);
										FlushToDisk(filename, message);
									}

									message = String.Format("[OK] Filename {0} has been purged because it's too old", Path.GetFileName(item));
									FlushToDisk(filename, message);

								}
							} catch {
							}
						}
					}
				}
			} catch (Exception) {
			}
		}
		#endregion
		#region FlushToDisk
		private static void FlushToDisk(string filename, string text) {
			try {
				string messagetowrite = String.Format("{0} - {1}\r\n", DateTime.Now.ToString("dd/MM/yyyy HH.mm.ss.fff"), text);
				File.AppendAllText(filename, messagetowrite);

			} catch (Exception) {

			}
		}
		#endregion
		#region WriteToConsole
		private static void WriteToConsole(string message) {
			Console.WriteLine("{0} - {1}", DateTime.Now.ToString("dd/MM/yyyy HH.mm.ss.fff"), message);
			return;
		}
		#endregion

		#region Write
		public static void Write(string message, LogLocation location, string folder = "logs") {
			/*if (ConfigurationManager.AppSettings["EnableLogs"] == null)
					return;*/

			switch (location) {
				case LogLocation.EnvetViewer:
					WriteToEventViewer(message);
					break;
				case LogLocation.Disk:
					WriteToDisk(message, folder);
					break;
				case (LogLocation.Console | LogLocation.Disk):
					WriteToConsole(message);
					WriteToDisk(message, folder);
					break;
				case (LogLocation.EnvetViewer | LogLocation.Disk):
					WriteToEventViewer(message);
					WriteToDisk(message, folder);
					break;
				case LogLocation.Database:
					break;
				case LogLocation.Console:
					WriteToConsole(message);
					break;
			}
		}
		#endregion

	}
}
