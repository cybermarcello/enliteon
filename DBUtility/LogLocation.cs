﻿using System;

namespace EnliteonLibraries.Common {
	public enum LogLocation : int {
		EnvetViewer = 1,
		Disk = 2,
		Database = 4,
		Console = 8
	}
}
